TARGET = pudding
FLAGS = -Wall -threaded -rtsopts
HC = ghc

all: build

build:
	$(HC) $(FLAGS) Main.hs -o $(TARGET)

clean:
	-rm -f *.o *.hi *.hc

vim:
	vim *.hs Makefile

.PHONY: all clean build vim
