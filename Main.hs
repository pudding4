module Main where

import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Data.IORef
import Data.Time.Clock
import Fermions

tick :: NominalDiffTime
tick = 0.025

defaultFermionNum :: Float
defaultFermionNum = 100

boxTransform :: Vector -> Vector
boxTransform (x,y,z) = (x/boxSpan, y/boxSpan, z/boxSpan)

pointerTransform :: Size -> (Float, Float) -> Vector
pointerTransform (Size w h) (x, y) = (((2 * x / fromIntegral w) - 1) * boxSpan, (1 - (2 * y / fromIntegral h)) * boxSpan, 0)

main :: IO ()
main = do 
  (_, args) <- getArgsAndInitialize
  _ <- createWindow "Pudding!"
  particles <- newIORef (initialState (if null args then defaultFermionNum else (read (head args))))
  t <- getCurrentTime
  lastUpdate <- newIORef t
  displayCallback $= (display particles)
  idleCallback $= Just (idle particles lastUpdate)
  keyboardMouseCallback $= Just (keyboardMouse particles)
  reshapeCallback $= Just reshape
  mainLoop

display :: IORef [Fermion] -> IO ()
display particleRef = do 
  particles <- readIORef particleRef
  --putStrLn (show particles)
  clear [ColorBuffer]
  renderPrimitive Points $ mapM_ getVertex (map (boxTransform . getFermionPos) particles)
  flush

keyboardMouse :: (Show a, Show b) => IORef [Fermion] -> Key -> a -> b -> Position -> IO()
keyboardMouse particleRef (Char '1') _ _ (Position x y) = do
  viewport' <- get viewport
  particles <- readIORef particleRef
  let (_, size) = viewport'
      pos = (pointerTransform size (fromIntegral x, fromIntegral y))
    in do
      putStr "Number of particles now: "
      putStrLn (show (length particles + 1))
      writeIORef particleRef ((pos, (0,0,0), 10, 0) : particles)
keyboardMouse _ _ _ _ _ = return ()

reshape :: Size -> IO ()
reshape s = do
  viewport $= (Position 0 0, s)

idle :: IORef [Fermion] -> IORef UTCTime -> IO ()
idle particleRef updateTimeRef = do
  lastUpdate <- readIORef updateTimeRef
  t <- getCurrentTime
  if (t `diffUTCTime` lastUpdate) > tick
     then do
        particles <- readIORef particleRef
        writeIORef particleRef (advanceState particles)
        writeIORef updateTimeRef t
        postRedisplay Nothing
  else
    return ()

getVertex :: Vector -> IO ()
getVertex (x, y, z) = vertex $ Vertex3 x y z
