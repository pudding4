module Fermions where

type Charge = Float
type Mass = Float
type Vector = (Float, Float, Float)
type Fermion = (Vector, Vector, Mass, Charge)

inBox :: Fermion -> Bool
inBox ((x, y, z), _, _, _) = x <= boxSpan && x >= (-boxSpan) && y <= boxSpan && y >= (-boxSpan) && z <= boxSpan && z >= (-boxSpan)

boxSpan :: Float
boxSpan = 1000

initialState :: Float -> [Fermion]
initialState n = map (\k -> (((k * boxSpan / (n+1)) - boxSpan/2, (sin(pi*k/n)) / 6 * boxSpan, 0), (0, 0, 0), 2, (if (odd (round k::Integer)) then 1 else -1))) [1..n]

getFermionPos :: Fermion -> Vector
getFermionPos (pos, _, _, _) = pos

addVectors :: Vector -> Vector -> Vector
addVectors (x,y,z) (a,b,c) = (x+a, y+b, z+c)

mulVector :: Float -> Vector -> Vector
mulVector n (x,y,z) = (n*x, n*y, n*z)

advanceState :: [Fermion] -> [Fermion]
advanceState fermions = filter inBox (map update fermions)
  where
  update this@(pos, v, mass, charge) = (newPos, newVelocity, mass, charge)
    where
    newPos = pos `addVectors` v
    newVelocity = updatedVelocity --`addVectors` (forceFromBlackHole this)
    updatedVelocity = (foldl addVectors v (map (forceBetween this) fermions))

forceBetween :: Fermion -> Fermion -> Vector
forceBetween (pos1@(x1, y1, z1), _, mass1, charge1) (pos2@(x2, y2, z2), _, mass2, charge2)
  | pos1 == pos2 = (0, 0, 0)
  | otherwise    = (factor * x, factor * y, factor * z)
  where
    factor = (-charge1) * charge2 / dist +  mass1 * mass2 / dist
    dist = x*x + y*y + z*z
    x = x2 - x1
    y = y2 - y1
    z = z2 - z1

--forceFromBlackHole :: Fermion -> Vector
--forceFromBlackHole ((x, y, z), _, _, _) = (factor * x, factor * y, factor * z)
--  where
--  factor = if dist > boxSpan then -0.1 else 0
----  factor = if factor' == 1/0 then 10000 else factor'
----  factor' = (-(exp ((dist - boxSpan)/boxSpan)))
--  dist = sqrt(x*x + y*y + z*z)
